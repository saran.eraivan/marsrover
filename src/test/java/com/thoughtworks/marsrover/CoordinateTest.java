package com.thoughtworks.marsrover;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CoordinateTest {
    @Test
    void shouldAddCoordinateWithXAndYValues() {
        Coordinate coordinate = new Coordinate(1, 2);
        Coordinate expected = new Coordinate(1, 3);

        assertEquals(expected, coordinate.add(0, 1));
    }
}