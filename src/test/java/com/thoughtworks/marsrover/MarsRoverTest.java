package com.thoughtworks.marsrover;

import com.thoughtworks.marsrover.exceptions.InvalidOperationException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MarsRoverTest {

    @Test
    void shouldCreateMarsRoverWithCoOrdinatesAndDirection() {
        Coordinate coordinate = new Coordinate(1, 2);
        Direction direction = Direction.NORTH;
        MarsRover marsRover = new MarsRover(coordinate, direction);

        assertEquals("1,2 NORTH", marsRover.toString());
    }

    @Test
    void shouldTurnLeftWhenLIsGiven() throws InvalidOperationException {
        Coordinate coordinate = new Coordinate(1, 2);
        Direction direction = Direction.NORTH;
        MarsRover marsRover = new MarsRover(coordinate, direction);

        marsRover.operate('L');

        assertEquals("1,2 WEST", marsRover.toString());
    }

    @Test
    void shouldTurnRightWhenLIsGiven() throws InvalidOperationException {
        Coordinate coordinate = new Coordinate(1, 2);
        Direction direction = Direction.NORTH;
        MarsRover marsRover = new MarsRover(coordinate, direction);

        marsRover.operate('R');

        assertEquals("1,2 EAST", marsRover.toString());
    }

    @Test
    void shouldMoveOneStepForwardWhenMIsGiven() throws InvalidOperationException {
        Coordinate coordinate = new Coordinate(1, 2);
        Direction direction = Direction.NORTH;
        MarsRover marsRover = new MarsRover(coordinate, direction);

        marsRover.operate('M');

        assertEquals("1,3 NORTH", marsRover.toString());
    }

    @Test
    void shouldThrowExceptionWhenInvalidCommandIsGiven() {
        Coordinate coordinate = new Coordinate(1, 2);
        Direction direction = Direction.NORTH;
        MarsRover marsRover = new MarsRover(coordinate, direction);

        assertThrows(InvalidOperationException.class, () -> marsRover.operate('A'));
    }

    @Test
    void shouldPerformSeriesOfInstructionsWhenGiven() throws InvalidOperationException {
        Coordinate coordinate = new Coordinate(1, 2);
        Direction direction = Direction.NORTH;
        MarsRover marsRover = new MarsRover(coordinate, direction);
        String commands = "LMLMLMLMM";

        marsRover.operate(commands);

        assertEquals("1,3 NORTH", marsRover.toString());
    }
}
