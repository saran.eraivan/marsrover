package com.thoughtworks.marsrover;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DirectionTest {

    @Test
    void shouldTurnLeft() {
        Direction direction = Direction.NORTH;

        assertEquals(Direction.WEST, direction.left());
    }

    @Test
    void shouldTurnRight() {
        Direction direction = Direction.NORTH;

        assertEquals(Direction.EAST, direction.right());
    }
}