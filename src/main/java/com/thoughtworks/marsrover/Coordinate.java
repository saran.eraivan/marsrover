package com.thoughtworks.marsrover;

import java.util.Objects;

public class Coordinate {
    private final int xPosition;
    private final int yPosition;

    public Coordinate(int xPosition, int yPosition) {
        this.xPosition = xPosition;
        this.yPosition = yPosition;
    }

    public Coordinate add(int xValue, int yValue) {
        return new Coordinate(xPosition + xValue, yPosition + yValue);
    }

    @Override
    public String toString() {
        return xPosition + "," + yPosition;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinate that = (Coordinate) o;
        return xPosition == that.xPosition && yPosition == that.yPosition;
    }

    @Override
    public int hashCode() {
        return Objects.hash(xPosition, yPosition);
    }
}
