package com.thoughtworks.marsrover;

import com.thoughtworks.marsrover.exceptions.InvalidOperationException;

public class MarsRover {
    private Coordinate coordinate;
    private Direction direction;

    public MarsRover(Coordinate coordinate, Direction direction) {
        this.coordinate = coordinate;
        this.direction = direction;
    }

    public void operate(String commands) throws InvalidOperationException {
        char[] commandArray = commands.toCharArray();
        for (char command : commandArray) operate(command);
    }

    public void operate(char command) throws InvalidOperationException {
        switch (command) {
            case 'L' -> turnLeft();
            case 'R' -> turnRight();
            case 'M' -> moveForwardInDirection();
            default -> throw new InvalidOperationException();
        }
    }

    @Override
    public String toString() {
        return coordinate + " " + direction;
    }

    private void turnLeft() {
        this.direction = direction.left();
    }

    private void turnRight() {
        this.direction = direction.right();
    }

    private void moveForwardInDirection() {
        int[][] movement = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
        int xMovement = movement[direction.ordinal()][0];
        int yMovement = movement[direction.ordinal()][1];
        this.coordinate = coordinate.add(xMovement, yMovement);
    }
}
